SHELL=/bin/bash

SRC_DIR=src
BIN_DIR=bin

CC=cc
#CFLAGS=-Wall --pedantic
LDFLAGS=

FIND_OPTS=-mindepth 1 -maxdepth 1 -type f

.PHONY:	clean all

all:
	find . $(FIND_OPTS) -name '*.c' | xargs -r basename -a -s .c | xargs -r $(MAKE)

clean:
	find . $(FIND_OPTS) -executable -print -delete

%: %.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $<

